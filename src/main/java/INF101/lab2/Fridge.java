package INF101.lab2;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    ArrayList<FridgeItem> fridge;

    public Fridge() {
        fridge = new ArrayList<FridgeItem>();
    }
    
    @Override
    public int nItemsInFridge() {
        int size = fridge.size();
        return size;
    }

    int maxsize = 20;
    
    @Override
    public int totalSize() {
        return maxsize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            fridge.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)) {
            fridge.remove(item);
        } else {
            throw new NoSuchElementException(); 
        }
    }

    @Override
    public void emptyFridge() {
        fridge.clear();
    }

    @Override
    public ArrayList<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = ExpiredItems();
        fridge.removeAll(expiredItems);
        return expiredItems;
    }

    private ArrayList<FridgeItem> ExpiredItems() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for (FridgeItem item : fridge) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        return expiredItems;

    }


}